# Bicara Tagging Plugin

Bicara Tagging Plugin allows session hosts to tag interest levels during a session to support using the tagging metadata for facilitating the curation asset generation process.

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
